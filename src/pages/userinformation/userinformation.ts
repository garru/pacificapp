import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { RestProvider } from '../../providers/rest/rest';
import * as moment from 'moment';
import { ChangeInfoPage } from '../change-info/change-info';
/**
 * Generated class for the UserinformationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-userinformation',
  templateUrl: 'userinformation.html',
})
export class UserinformationPage {
  public account: any;
  private getUserUrl = 'https://pacificgo.asia/secret/users/'
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public restProvider: RestProvider
  ) {
    moment.locale('vi');
    
  }

  ionViewWillEnter() {
    this.getUser();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UserinformationPage');
  }

  gotoChangeInfo() {
    this.navCtrl.push(ChangeInfoPage, {userInfo: this.account})
  }

  getUser(referesher?) {
    this.restProvider.showLoading();
    this.account = this.restProvider.getUserInfor();
    this.restProvider.GET(
      '', null,
      this.getUserUrl + this.account._id,
      null, true
    ).subscribe(res => {
      this.restProvider.hideLoading();
      referesher && referesher.complete();
      if (res && res.data && res.data.User) {
        this.account = res.data.User;
        moment.locale('vi');
        this.account.amountText = this.restProvider.formatCurrency(this.account.amount);
        this.account.created_at && (this.account.created_atText = moment(this.account.created_at).format("DD/MM/YYYY"));
        this.account.birthday && (this.account.birthdayText = moment(this.account.birthday).format("DD/MM/YYYY"));
      }
      switch (this.account.userRoleId) {
        case 1:
          this.account.position = "Admin";
          break;
        case 2:
          this.account.position = "Đối tác kinh doanh";
          break;
        case 3:
          this.account.position = "Khách hàng";
          break;
        case 4:
          this.account.position = "Thành viên";
          break;
      }
      switch (this.account.gender) {
        case 0:
          this.account.genderText = "Nam";
          break;
        case 1:
          this.account.genderText = "Nữ";
          break;
        case 2:
          this.account.genderText = "LGBT";
          break;
      }
    }, err => {
      referesher && referesher.complete();
      this.restProvider.hideLoading();
      this.restProvider.showAlert(err.message);
    })
    if (this.account.birthday) {
      this.account.birthday = moment(this.account.birthday).format("DD MMMM YYYY");
    }
    if (this.account.created_at) {
      this.account.created_at = moment(this.account.created_at).format("DD MMMM YYYY");
    }
  }
}
