import { Component } from '@angular/core';
import { NavController, NavParams, ModalController } from 'ionic-angular';
import { DetailHistoryPage } from '../detail-history/detail-history';
import { RestProvider } from '../../providers/rest/rest';
import * as moment from 'moment';
import { FilterPage } from '../filter/filter';
/**
 * Generated class for the HistoryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-history',
  templateUrl: 'history.html',
})
export class HistoryPage {
  public currentMonth = moment().month() + 1;
  public currentYear = moment().year();
  private currentTime = moment().format("YYYY-MM-DD");
  public urlTransactions = "https://pacificgo.asia/secret/commit/transactions";
  public searchTransactions = "https://pacificgo.asia/secret/transactions/search/";
  public listTransaction: any = null;
  public userId: number;
  public length: any;
  public userRoleId: number;
  public listTransactionCurrent: any;
  public listTransactionFilter: any;
  public onSearch = false;
  public onFilter = false;
  public searchText = "";
  public dataFilter = {
    monthYear: this.currentTime
  };
  public totalAmount;
  public totalAmountText;
  public listTransactionSort = {};
  public showTime;
  public timer;
  public listSearch = [];
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private restProvider: RestProvider,
    public modalController: ModalController
  ) {
    this.userId = this.restProvider.getUserInforItem('_id')
    this.userRoleId = this.restProvider.getUserInforItem('userRoleId');
    // this.urlTransactions += this.userId + '/transactions';
    this.getListTransition();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HistoryPage');
  }

  gotoHistoryDetai(item) {
    this.navCtrl.push(DetailHistoryPage, { data: item, userRoleId: this.userRoleId });
  }

  search() {
    clearTimeout(this.timer);
    if (this.searchText.length >= 3) {
      this.timer = setTimeout(() => {
        this.listSearch = [];
        this.listTransactionCurrent = [];
        let searchParam = this.generateFilterData(this.searchText);
        this.restProvider.GET("", null, this.urlTransactions + searchParam, null, true).subscribe(res => {
          if (res && res.data && res.data.items && res.data.items.length) {
            res.data.items.forEach((item) => {
              !(item.name in this.listSearch) && this.listSearch.push(item);
            })
            this.listTransactionFilter = res.data.items;
          }
        }, err => {
          this.restProvider.showAlert(err.message)
        })
      }, 500);
    } else if (this.searchText.length === 0) {
      this.timer = setTimeout(() => {
        this.listSearch = [];
        this.getListTransition();
      }, 500);
    }
  }

  filter() {
    let filterPage = this.modalController.create(FilterPage, { dataFilter: this.dataFilter });
    filterPage.present();
    filterPage.onDidDismiss(data => {
      data && this.doFilter(data);
    })
  }

  clearSearch() {
    this.onSearch = false;
    this.searchText = "";
    this.listSearch = [];
    this.getListTransition();
  }
  toggleSearch() {
    this.onSearch = !this.onSearch;
    if (!this.onSearch) {
      this.clearSearch();
    }
  }

  doRefresh(referesher) {
    this.getListTransition(referesher);
  }

  getListTransition(referesher?) {
    this.restProvider.showLoading();
    let searchParam = this.generateFilterData();
    this.totalAmount = 0;
    this.restProvider.GET('', null, this.urlTransactions + searchParam, false, true).subscribe(res => {
      this.restProvider.hideLoading();
      referesher && referesher.complete();
      if (res && res.data) {
        this.listTransaction = res.data.items;

        this.formatTransactionList();
        this.totalAmountText = this.totalAmount.toLocaleString('it-IT', { style: 'currency', currency: 'VND' });

      } else {
        this.listTransaction = null;
        this.listTransactionCurrent = null;
        this.listTransactionFilter = null;
      }
    }, err => {
      this.restProvider.hideLoading();
      referesher && referesher.complete();
      this.restProvider.showAlert(err.message);
    })
  }

  doFilter(data) {
    this.dataFilter = data;
    this.currentMonth = moment(data.monthYear).month() + 1;
    this.currentYear = moment(data.monthYear).year();
    this.clearSearch();
  }

  generateFilterData(textSearch?) {
    let time = moment(this.dataFilter.monthYear).format();
    const startOfMonth = moment(time, "YYYY-MM-DD").utc(true).startOf('month').unix();
    const endOfMonth = moment(time).endOf('month').utc(true).unix();
    return '?' + 'from=' + startOfMonth + '&to=' + endOfMonth + (textSearch ? ("&term=" +textSearch) : '');
  }

  selectSearch(id) {
    this.restProvider.showLoading();
    this.restProvider.GET('', null, this.urlTransactions + '/' + id, false, true).subscribe(res => {
      this.restProvider.hideLoading();
      if (res.data && res.data.items) {
        this.listTransaction = res.data.items;
        this.formatTransactionList();
        setTimeout(() => {
          this.listSearch = [];
          this.onSearch = false;
        }, 100)
      }
    }, err => {
      this.restProvider.hideLoading();
      this.restProvider.showAlert(err.message);
    });
  }

  formatTransactionList() {
    moment.locale('vi');
    this.listTransactionSort = {};
    this.listTransaction.forEach((item, idx) => {
      item.sortDate = item.created_at;
      item.sortDateText = moment.unix(item.created_at).format("DD/MM/YYYY");
      !(item.sortDateText in this.listTransactionSort) && (this.listTransactionSort[item.sortDateText] = []);
      this.listTransactionSort[item.sortDateText].push(item);
      item.created_at &&
        (this.listTransaction[idx].created_atText =
          moment(moment.unix(item.created_at).format()).format("hh:mm  DD/MM/YYYY"));
      item.update_at &&
        (this.listTransaction[idx].update_atText =
          moment(moment.unix(item.update_at).format()).format("hh:mm  DD/MM/YYYY"));
      item.amountPayment && (item.discountText = item.amountPayment.toLocaleString('it-IT', { style: 'currency', currency: 'VND' }));
      item.introducer_amount && (item.introducerText = item.introducer_amount.toLocaleString('it-IT', { style: 'currency', currency: 'VND' }));
      item.member_amount && (item.memberText = item.member_amount.toLocaleString('it-IT', { style: 'currency', currency: 'VND' }));
      item.pacific_amount && (item.pacificText = item.pacific_amount.toLocaleString('it-IT', { style: 'currency', currency: 'VND' }));
      // Chia type giao dịch
      if (this.userRoleId === 2) {//partner
        item.type = 1; //Nhận tiền thanh toán bởi customer
      } else if (this.userRoleId === 4) {//member
        (item.customer_id === this.userId && item.member_id === this.userId) && (item.type = 2);// Dùng thẻ chính mình thanh toán và nhận đưọc hoa hồng tích luỹ
        (item.introducer_id === this.userId && item.member_id !== this.userId && item.customer_id !== this.userId) && (item.type = 3);// Nhận hoa hồng liên kết từ partner nhờ việc giới thiệu
        (item.introducer_id !== this.userId && item.member_id === this.userId && item.customer_id !== this.userId) && (item.type = 4);// Nhận hoa hồng liên kết
        (item.customer_id === this.userId && item.member_id !== this.userId) && (item.type = 5);// Thanh toán bằng mã thẻ nguời khác
        (item.introducer_id === this.userId && item.member_id === this.userId && item.customer_id !== this.userId) && (item.type = 6);// Dùng thẻ chính mình thanh toán và nhận đưọc hoa hồng tích luỹ
      } else {
        item.type = 5; // Customer thanh toán
      }
      // Tổng số tiền giao dịch
      ((this.userRoleId === 2 && item.type === 1) || // Tổng số tiền nhận được
      (this.userRoleId === 4 && (item.type === 5 || item.type ===2)) || // Member chi tiêu
      (this.userRoleId === 3)) && // Customer chi tiêu
      (this.totalAmount += item.amountPayment);
    })
    this.listTransactionCurrent = [];
    this.showTime = [];
    for (let o in this.listTransactionSort) {
      this.listTransactionCurrent.push(this.listTransactionSort[o]);
      this.showTime.push(o);
    }
    this.length = this.listTransaction.length;
    this.listTransactionFilter = this.listTransactionCurrent;
  }

}
