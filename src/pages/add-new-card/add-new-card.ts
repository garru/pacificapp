import { Component, ApplicationRef } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { RestProvider } from '../../providers/rest/rest';
/**
 * Generated class for the AddNewCardPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-add-new-card',
  templateUrl: 'add-new-card.html',
})
export class AddNewCardPage {
  public wallet: FormGroup;
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private formBuilder: FormBuilder,
    private elementRef: ApplicationRef,
    private rest: RestProvider
    ) {
      this.wallet = this.formBuilder.group({
        cardNumber: ['', [Validators.required]],
        expiryDate: ['', [Validators.required]],
        cvv: [, [Validators.required]],
        name: ['', [Validators.required]]
      })
      
      this.wallet.controls.cardNumber.valueChanges.subscribe(value => {
        
        let creditNo = this.rest.formatCreditCardNumber(value); // remove all space
        this.wallet.controls.cardNumber.setValue(creditNo, {emitEvent: false});
      })

      this.elementRef.tick();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddCardPage');
  }

  submit() {
    this.navCtrl.pop();
  }

}
