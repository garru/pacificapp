import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Platform, Navbar } from 'ionic-angular';
import { ProductDetailPage } from '../product-detail/product-detail';
import {RestProvider} from '../../providers/rest/rest';

@Component({
  selector: 'page-list',
  templateUrl: 'list.html'
})
export class ListPage {

  @ViewChild(Navbar) navBar: Navbar;

  public productList : any;
  public urlProduct = '/api/v1/products';

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public platform: Platform,
    private restProvider: RestProvider
  ) {

    this.restProvider.GET(this.urlProduct, null, null, null, true).subscribe(res => {
      this.restProvider.hideLoading();
      if (res && res.data) {
        this.productList = res.data.items;
        this.productList.forEach(item => {
          item['productTN'] = 'https://pacificgo.asia' + item['productTN'] ;
        })
      } else {
        this.productList = null;
      }
    }, error => {
      this.restProvider.hideLoading();
      this.restProvider.showAlert(error.message);
    })
  }

  ionViewWillEnter() {
    this.navBar.backButtonClick = () => {
      this.navCtrl.pop();
    }
  }

  gotoDetail(item) {
    this.navCtrl.push(ProductDetailPage, {data: item});
  }
}
