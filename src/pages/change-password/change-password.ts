import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ValidationService } from '../../share/validation';
import { RestProvider } from '../../providers/rest/rest';
import { SuccessPage } from '../success/success';
import { Observable } from 'rxjs';

/**
 * Generated class for the ChangePasswordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-change-password',
  templateUrl: 'change-password.html',
})
export class ChangePasswordPage {
  public changePasswordForm: FormGroup;
  public messErrorNewPassword: string = "";
  public messErrorOldPassword: string = "";
  private urlChangePassword = "secret/users/";
  private userId: number;
  private ulrValidatePassword = "/api/v1/validatePassword";
  public showPass = {
    oldPass: false,
    newPass: false,
    confirm: false
  }
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private formBuilder: FormBuilder,
    private resProvider: RestProvider,
    private validationService: ValidationService
  ) {
    this.changePasswordForm = this.formBuilder.group({
      oldPassword: ['', [Validators.required, Validators.minLength(8)]],
      newPassword: ['', [Validators.required, Validators.minLength(8)]],
      confirmNewPassword: ['', [Validators.required, Validators.minLength(8)]],
    })
    this.userId = this.resProvider.getUserInfor()['_id'];
    this.urlChangePassword += this.userId + '/password';
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChangePasswordPage');
  }

  submit() {
    this.messErrorNewPassword = this.validateNewPassword();
    if (this.changePasswordForm.valid && !this.messErrorNewPassword) {
      this.resProvider.showLoading();
      this.resProvider.PUT(
        this.urlChangePassword,
        {
          "new_password": this.changePasswordForm.controls.newPassword.value,
          "old_password": this.changePasswordForm.controls.oldPassword.value
        },
        "",
        true,
        true
      ).subscribe(res => {
        this.resProvider.hideLoading();
        if (res['success']) {
          this.navCtrl.setRoot(SuccessPage, { title: "Thay đổi mật khẩu thành công" , islogin: true});
        } else {
          this.resProvider.showAlert(res['message']);
        }
      }, err => {
        this.resProvider.hideLoading();
        this.resProvider.showAlert(err.message);
      })
    } else {
      this.validationService.validateAllFields(this.changePasswordForm);
    }
  }

  validateOldPassword = (): Observable<any> => {
    return this.resProvider.POST(
      this.ulrValidatePassword,
      { userId: this.userId, password: this.changePasswordForm.controls.oldPassword.value },
      "",
      true
    );
  }

  validateNewPassword() {
    let mess = "";
    if (this.changePasswordForm.controls.newPassword.value === this.changePasswordForm.controls.oldPassword.value) {
      mess = "Mật khẩu mới không được trùng với mật khẩu cũ";
    }
    if (this.changePasswordForm.controls.newPassword.value === this.changePasswordForm.controls.confirmNewPassword.value) {
      mess = ""
    } else {
      mess = "Xác nhận mật khẩu mới không đúng";
    }
    return mess;
  }

  displayPassword(key) {
    this.showPass[key] = !this.showPass[key];
  }

}
