import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ValidationService } from '../../share/validation';
import { SuccessPage } from '../success/success';
import { RestProvider } from '../../providers/rest/rest';
import * as _ from 'lodash';

/**
 * Generated class for the WithdrawalRequestPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-withdrawal-request',
  templateUrl: 'withdrawal-request.html',
})
export class WithdrawalRequestPage {
  public withdrawalRequest: FormGroup;
  private userId: number;
  public alertMessage: string;
  public listMonth = [];
  public bankInfor = {}
  public currentMonth = {};

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private formBuilder: FormBuilder,
    private validationService: ValidationService,
    private rest: RestProvider
  ) {
    let userInfor = this.rest.getUserInfor();
    this.userId = userInfor._id;
    this.bankInfor = {
      "BankName": userInfor.BankName,
      "BankAddress": userInfor.BankAddress,
      "BankAccountHolder": userInfor.BankAccountHolder,
      "BankAccount": userInfor.BankAccount,
    }
    this.rest.showLoading();
    this.rest.GET('secret/user/listMonth?member_id=' + this.userId, null, '', true, true).subscribe(res => {
      this.rest.hideLoading();
      if(res.data) {
        let listMonth = _.get(res.data, 'listMonth');
        if (_.isNumber(listMonth)) {
          this.alertMessage = 'Quý khách không đủ điều kiện rút tiền!';
          this.rest.showAlert(this.alertMessage);
        } else {
          if(listMonth.length === 0) {
            this.alertMessage = 'Quý khách đã rút hết tiền trong ví!';
            this.rest.showAlert(this.alertMessage);
          } else {
            this.listMonth = [];
            listMonth.forEach(item => {
              const month = item.split(':');
              this.listMonth.push({
                month: month[0],
                money: this.rest.formatCurrency(month[1]) + ' đ'
              })
            })

          }
        }
      } else {
        this.rest.showAlert(res.message);
      }
    }, err => {
      this.rest.hideLoading();
      this.rest.showAlert(err.message);
    })
    this.withdrawalRequest = this.formBuilder.group({
      time: ['', [Validators.required]]
    })
  }

  selectMonth(month) {
    this.currentMonth = month;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad WithdrawalRequestPage');
  }
  submit() {
    this.validationService.validateAllFields(this.withdrawalRequest);

    if (this.alertMessage) {
      this.rest.showAlert(this.alertMessage);
    }
    else {
      if (this.withdrawalRequest.valid) {
        const time = this.withdrawalRequest.controls.time.value;
        const monthday = time.split('/')[0];
        const yearfull = time.split('/')[1];
        const param = {
          userId: this.userId,
          monthday: monthday,
          yearfull: yearfull
        }
        this.rest.POST(`secret/user/suggest`, param, '', true, true).subscribe(res => {
          this.rest.hideLoading();
          if (res['data'] && res['data'].data && res['data'].data[0] > 0) {
            this.withdrawalRequest.valid && this.navCtrl.setRoot(SuccessPage, { title: "GỞI YÊU CẦU RÚT TIỀN THÀNH CÔNG" });
          } else {
            this.rest.showAlert('Gởi yêu cầu rút tiền không thành công.');
          }
        }, err => {
          this.rest.hideLoading();
          this.rest.showAlert(err.message);
        })
      }
    }

  }
}