import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ValidationService } from '../../share/validation';
import { RestProvider } from '../../providers/rest/rest';
import { HomePage } from '../home/home';
import { RegisterPage } from '../register/register';
import { ForgotPasswordPage } from '../forgot-password/forgot-password';

/**
 * Generated class for the Login_2Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  public loginForm: FormGroup;
  // private urlLogin = 'secret/login';
  public showPass = false;
  public onInput = false;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private formBuilder: FormBuilder,
    public validationService: ValidationService,
    private restProvider: RestProvider
  ) {
    this.loginForm = this.formBuilder.group({
      userName: ['', [Validators.required]],
      password: ['', [Validators.required]]
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  onSubmit() {
    if (this.loginForm.valid) {
      let param = {
        username: this.loginForm.controls.userName.value,
        password: this.loginForm.controls.password.value
      }
      this.restProvider.showLoading();
      this.restProvider.POST('', param, 'https://pacificgo.asia/secret/login', true, true).subscribe(res => {
        if(res['success'] && res['data'] && res['data']['User'] && res['data']['User']['otp_validate']) {
          this.restProvider.setUser(res);
          this.navCtrl.setRoot(HomePage);
          this.restProvider.hideLoading();
        } else {
          this.restProvider.showAlert(res['message']);
          this.restProvider.hideLoading();
        }
      },
    err => {
      this.restProvider.showAlert(err.message);
      this.restProvider.hideLoading();
    })
    } else {
      this.validationService.validateAllFields(this.loginForm);
    }
  }

  displayPassword() {
    this.showPass = !this.showPass;
  }

  focusIn() {
    this.onInput = true;
  }
  
  focusOut() {
    this.onInput = false;
  }

  gotoRegister() {
    this.navCtrl.push(RegisterPage);
  }
  
  gotoForgotPassword() {
    this.navCtrl.push(ForgotPasswordPage);
  }

}