import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ValidationService } from '../../share/validation';
import { RestProvider } from '../../providers/rest/rest';
import { HomePage } from '../home/home';

/**
 * Generated class for the Login_2Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-login-2',
  templateUrl: 'login-2.html',
})
export class Login_2Page {

  public loginForm: FormGroup;
  private urlLogin = '/secret/login';
  public showPass = false;
  public onInput = false;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private formBuilder: FormBuilder,
    public validationService: ValidationService,
    private restProvider: RestProvider
  ) {
    this.loginForm = this.formBuilder.group({
      userName: ['', [Validators.required]],
      password: ['', [Validators.required]]
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  onSubmit() {
    if (this.loginForm.valid) {
      let param = {
        username: this.loginForm.controls.userName.value,
        password: this.loginForm.controls.password.value
      }
      this.restProvider.showLoading();
      this.restProvider.POST(this.urlLogin, param, '', true, true).subscribe(res => {
        if(res['success']) {
          this.restProvider.setUser(res);
          this.navCtrl.setRoot(HomePage);
          this.restProvider.hideLoading();
        } else {
          this.restProvider.showAlert(res['message']);
          this.restProvider.hideLoading();
        }
      },
    err => {
      this.restProvider.showAlert(err.message);
      this.restProvider.hideLoading();
    })
    } else {
      this.validationService.validateAllFields(this.loginForm);
    }
  }

  displayPassword() {
    this.showPass = !this.showPass;
  }

  focusIn() {
    this.onInput = true;
  }
  
  focusOut() {
    this.onInput = false;
  }

}
