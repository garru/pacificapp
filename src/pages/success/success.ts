import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';
import { RestProvider } from '../../providers/rest/rest';
import { LoginPage } from '../login/login';

/**
 * Generated class for the SuccessPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-success',
  templateUrl: 'success.html',
})
export class SuccessPage {
  public title : string;
  public id: string;
  public islogin = this.navParams.get('islogin');
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public restProvider: RestProvider
  ) {
    this.title = this.navParams.get('title');
    this.id = this.navParams.get('id');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SuccessPage');
  }

  gotoHomePage(){
    this.restProvider.fakeLoading();
    if(this.islogin) {
      this.navCtrl.setRoot(LoginPage);
      window.localStorage.removeItem('TOKEN');
      window.localStorage.removeItem('USER_INFOR');
    } else {
      this.navCtrl.setRoot(HomePage);
    }
  }

}
