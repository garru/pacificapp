import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { RestProvider } from '../../providers/rest/rest';
import { SuccessPage } from '../success/success';

/**
 * Generated class for the OtpPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-otp',
  templateUrl: 'otp.html',
})
export class OtpPage {

  public otpArr = Array.from(Array(6).fill(null));
  public inputObj = {};
  public onResend = false;
  public enableSubmit = false;
  public error = false;
  private userId: any;
  private urlResendOTP = 'admin/api/v1/otp/resend';
  private urlValidateOTP = 'admin/api/v1/otp/validate';
  public onCountDown = false;
  public timeCountDownResendOtp = 30;
  public count;
  public phone;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public restProvider: RestProvider
  ) {
    this.userId = this.navParams.get('userId');
    this.phone = this.navParams.get('phone');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OtpPage');
  }

  submitOTP() {
    let otp = this.otpArr.join('');
    let param = {userId: this.userId, otp: otp}
    this.restProvider.POST(this.urlValidateOTP, param, '', true).subscribe( res => {
      if(res['success']) {
        this.navCtrl.setRoot(SuccessPage, {title: "ĐĂNG KÝ THÀNH CÔNG", islogin: true});
      } else {
        this.restProvider.showAlert(res['message']);
      }
    }, err => {
      this.restProvider.showAlert(err.message);
    })
  }

  next(inputElement, index?) {
    if(index !== 5) {
      setTimeout(() => {
        inputElement.setFocus();
      }, 100)
    } else {
      setTimeout(() => {
        inputElement.setBlur();
      }, 100)
    }
    let otp = this.otpArr.join('');
    if (otp.length === 6) {
      this.enableSubmit = true;
    }
  }

  resendOTP(inputElement) {
    this.restProvider.POST(this.urlResendOTP, {userId: this.userId}, '', true).subscribe(res => {
      if(res['success']) {
        this.otpArr = Array.from(Array(6).fill(null));
        setTimeout(() =>{
          inputElement.setFocus();
          this.coutDownResend();
        }, 100);
      } else {
        this.restProvider.showAlert(res['message']);
      }
    }, err => {
      this.restProvider.showAlert(err.message);
    })
  }

  coutDownResend() {
    this.count = this.timeCountDownResendOtp;
    this.onCountDown = true;
    let timer = setInterval(() => {
      if(!this.count) {
        this.onCountDown = false;
        clearInterval(timer);
      } else {
        this.count --;
      }
    }, 1000);
  }

}
