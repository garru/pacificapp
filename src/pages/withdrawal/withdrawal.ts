import { Component } from '@angular/core';
import { NavController, NavParams, ModalController } from 'ionic-angular';
import { RestProvider } from '../../providers/rest/rest';
import * as moment from 'moment';
import { FilterPage } from '../filter/filter';
import { WithdrawalDetailPage } from '../withdrawal-detail/withdrawal-detail';

/**
 * Generated class for the WithdrawalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-withdrawal',
  templateUrl: 'withdrawal.html',
})
export class WithdrawalPage {

  public currentMonth = moment().month() + 1;
  public currentYear = moment().year();
  private currentTime = moment().format("YYYY-MM-DD");
  private getWithdrawalHistoryUrl = 'https://pacificgo.asia/secret/vtcpay/history?partner_id=';
  private getWithdrawalHistoryMemberUrl = 'https://pacificgo.asia/secret/user/history?userId=';
  public searchTransactions = "https://pacificgo.asia/secret/transactions/search/";
  public listTransaction: any = null;
  public userId: number;
  public length: any;
  public userRoleId: number;
  public listTransactionCurrent: any;
  public listTransactionFilter: any;
  public onSearch = false;
  public onFilter = false;
  public searchText = "";
  public dataFilter = {
    monthYear: this.currentTime
  };
  public totalAmount;
  public totalAmountText;
  public listTransactionSort = {};
  public showTime;
  public timer;
  public listSearch = [];
  public isPay = this.navParams.get("isPay");
  public isMember = this.navParams.get("isMember");
  // private currentPage = 0;
  // private totalPage;
  // private totalTransaction;
  // private quantityTransaction;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private restProvider: RestProvider,
    public modalController: ModalController
  ) {
    this.userId = this.restProvider.getUserInforItem('_id')
    this.userRoleId = this.restProvider.getUserInforItem('userRoleId');
    // this.urlTransactions += this.userId + '/transactions';
    this.getListTransition();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HistoryPage');
  }

  gotoWithdrawalDetai(item) {
    this.navCtrl.push(WithdrawalDetailPage, { data: item, userRoleId: this.userRoleId, isPay: this.isPay, isMember: this.isMember });
  }

  search() {
    clearTimeout(this.timer);
    if (this.searchText.length >= 3) {
      this.timer = setTimeout(() => {
        this.listSearch = [];
        this.listTransactionCurrent = [];
        this.restProvider.GET("", null, this.searchTransactions + this.searchText, null, true).subscribe(res => {
          if (res && res.data && res.data.items && res.data.items.length) {
            res.data.items.forEach((item) => {
              !(item.name in this.listSearch) && this.listSearch.push(item);
            })
            this.listTransactionFilter = res.data.items.filter(item => item.status === (this.isPay ? 1 : -2));
          }
        }, err => {
          this.restProvider.showAlert(err.message)
        })
      }, 500);
    } else if (this.searchText.length === 0){
      this.timer = setTimeout(() => {
        this.listSearch = [];
        this.getListTransition();
      }, 500);
    }
  }

  filter() {
    let filterPage = this.modalController.create(FilterPage, { dataFilter: this.dataFilter });
    filterPage.present();
    filterPage.onDidDismiss(data => {
      data && this.doFilter(data);
    })
  }

  clearSearch() {
    this.onSearch = false;
    this.searchText = "";
    this.listSearch = [];
    this.getListTransition();
  }
  toggleSearch() {
    this.onSearch = !this.onSearch;
    if (!this.onSearch) {
      this.clearSearch();
    }
  }

  doRefresh(referesher) {
    this.getListTransition(referesher);
  }

  getListTransition(referesher?) {
    this.restProvider.showLoading();
    this.totalAmount = 0;
    this.restProvider.GET('', null, this.isMember? this.getWithdrawalHistoryMemberUrl + this.userId: this.getWithdrawalHistoryUrl+ this.userId, false, true).subscribe(res => {
      this.restProvider.hideLoading();
      referesher && referesher.complete();
      // res.data.items = [
      //   {
      //     "id": 1,
      //     "userId": 1,
      //     "monthday": 2,
      //     "yearfull": 2019,
      //     "status": 1,
      //     "created_at": "2019-03-12T17:00:00.000Z",
      //     "updated_at": "2019-03-31T17:23:30.000Z",
      //     "bankname": "DONG A",
      //     "bankaddress": "CAN THO",
      //     "bankaccountholder": "NGUYEN VAN A",
      //     "bankaccount": "01234567890"
      //   }
      // ]
      if (res && res.data) {
        this.isMember && (this.listTransaction = res.data.items);
        !this.isMember && (this.listTransaction = res.data.items.filter(item => item.status === (this.isPay ? 1 : -2)));
        this.formatTransactionList();
        this.totalAmountText = Math.abs(this.totalAmount).toLocaleString('it-IT', { style: 'currency', currency: 'VND' });

      } else {
        this.listTransaction = null;
        this.listTransactionCurrent = null;
        this.listTransactionFilter = null;
      }
    }, err => {
      this.restProvider.hideLoading();
      referesher && referesher.complete();
      this.restProvider.showAlert(err.message);
    })
  }

  doFilter(data) {
    this.dataFilter = data;
    this.currentMonth = moment(data.monthYear).month() + 1;
    this.currentYear = moment(data.monthYear).year();
    this.clearSearch();
  }

  generateFilterData() {
    let time = moment(this.dataFilter.monthYear).format();
    const startOfMonth = moment(time, "YYYY-MM-DD").utc(true).startOf('month').unix();
    const endOfMonth = moment(time).endOf('month').utc(true).unix();
    return '&' + 'from=' + startOfMonth + '&to=' + endOfMonth;
  }

  selectSearch(id) {
    this.restProvider.showLoading();
    this.restProvider.GET('', null, this.getWithdrawalHistoryUrl + '/' + id, false, true).subscribe(res => {
      this.restProvider.hideLoading();
      if(res.data && res.data.items) {
        this.listTransaction = res.data.items;
        this.formatTransactionList();
        setTimeout(() => {
          this.listSearch = [];
          this.onSearch = false;
        }, 100)
      }
    }, err => {
      this.restProvider.hideLoading();
      this.restProvider.showAlert(err.message);
    });
    // this.listTransaction = this.listTransactionFilter.filter(item => item.name === name);
    // this.formatTransactionList();
    // setTimeout(() => {
    //   this.listSearch = [];
    //   this.onSearch = false;
    // }, 100)
  }

  formatTransactionList() {
    moment.locale('vi');
    this.listTransactionSort = {};
    this.listTransaction.forEach((item, idx) => {
      let time = moment(item.created_at).format("DD/MM/YYYY");
      item.sortDate = moment(time, "DD/MM/YYYY").valueOf();
      item.sortDateText = moment(item.created_at).format("DD/MM/YYYY");
      !(item.sortDateText in this.listTransactionSort) && (this.listTransactionSort[item.sortDateText] = []);
      this.listTransactionSort[item.sortDateText].push(item);
      item.created_at &&
        (item.created_atText =
          moment(moment(item.created_at).format()).format("HH giờ mm p\\hút ngày DD/MM/YYYY"));
      item.update_at &&
        (item.update_atText =
          moment(moment(item.update_at).format()).format("HH giờ mm p\\hút ngày DD/MM/YYYY"));
          item.amount = item.amount || 10000;
      item.amount && (item.discountText = Math.abs(item.amount).toLocaleString('it-IT', { style: 'currency', currency: 'VND' }));
      this.totalAmount += item.amount;
    })
    this.listTransactionCurrent = [];
    this.showTime = [];
    for (let o in this.listTransactionSort) {
      this.listTransactionCurrent.push(this.listTransactionSort[o]);
      this.showTime.push(o);
    }
    this.length = this.listTransaction.length;
    this.listTransactionFilter = this.listTransactionCurrent;
  }

}
