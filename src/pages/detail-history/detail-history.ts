import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { RestProvider } from '../../providers/rest/rest';
import * as moment from 'moment';
/**
 * Generated class for the DetailHistoryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-detail-history',
  templateUrl: 'detail-history.html',
})
export class DetailHistoryPage {
  public detailData : any;
  public userRoleId: any;
  public salerName : any;
  // private getUserInforUrl = 'secret/user/'

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public rest: RestProvider
  ) {
    this.userRoleId = this.rest.getUserInforItem('userRoleId');
    this.detailData = this.navParams.get('data');
    this.detailData.created_atText = 
    moment(moment.unix(this.detailData.created_at).format()).format("HH giờ mm p\\hút ngày DD/MM/YYYY");
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DetailHistoryPage');
  }

}
