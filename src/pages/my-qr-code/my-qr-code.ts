import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Slides, AlertController } from 'ionic-angular';
import { RestProvider } from '../../providers/rest/rest';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { Observable } from 'rxjs';
// import * as _ from "lodash";

/**
 * Generated class for the MyQrCodePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-my-qr-code',
  templateUrl: 'my-qr-code.html',
})
export class MyQrCodePage {
  @ViewChild(Slides) slides: Slides;
  public qrCodeImg: any;
  public qrCodeImgList = [];
  public currentIndex = 0;
  public myphoto: any;
  private userId : number;
  private userRole;
  private removeUrl = "https://pacificgo.asia/secret/customer/del-qr-images";
  private defaultImage = "fe74b080-587c-11e9-8b80-d3fac5e5417c.jpg";
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public restProvider: RestProvider,
    private camera: Camera,
    private alertController: AlertController
  ) {
    // this.restProvider.showLoading();
    this.userId = this.restProvider.getUserInfor()['_id'];
    this.userRole = this.restProvider.getUserInfor()['userRoleId'];
    if(this.userRole === 3){
      this.getListQR();
    } else {
      this.qrCodeImg =  'https://pacificgo.asia/secret/image/' + this.userId;
    }
  }

  ngAfterViewInit() {
    if (this.qrCodeImg) {
      this.initSlide();
      this.restProvider.hideLoading();
    } else {
      this.restProvider.hideLoading();
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MyQrCodePage');
  }

  slideChanged(idx) {
    this.currentIndex = idx;
    this.slides.slideTo(idx, 500);
  }

  uploadImage() {
    this.restProvider.showLoading();
    let url = "https://pacificgo.asia/secret/customer/qr-images";
    let postData = new FormData();
    postData.append('qr_image', this.myphoto);
    let data: Observable<any> = this.restProvider.POST('', postData, url, null, true);
    data.subscribe(res => {
      this.restProvider.hideLoading();
      if (res && res['data'] && res['data']['featured_image']) {
        this.getListQR();
      }
    }, () => {
      this.restProvider.hideLoading();
      this.restProvider.showAlert("Lỗi mạng, vui lòng thử lại sau!")
    })
  }

  getImage() {
    const options: CameraOptions = {
      quality: 70,
      destinationType: this.camera.DestinationType.DATA_URL,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      saveToPhotoAlbum: false
    }

    this.camera.getPicture(options).then((imageData) => {
      this.uploadImage();
    }, (err) => {
      // Handle error
    });
  }

  addFile($event) {
    this.myphoto = $event.target.files[0];
    this.uploadImage();
  }

  getListQR() {
    this.restProvider.showLoading();
    this.restProvider.GET('', null, 'https://pacificgo.asia/secret/list/qr-images', null, true).subscribe(res => {
      this.restProvider.hideLoading();
      if (res && res['data']) {
        res['data'].list && res['data'].list.unshift({name: this.defaultImage})
        this.qrCodeImgList = res['data'].list || [];
        this.initSlide();
      }
    }, () => {
      this.restProvider.hideLoading();
      this.restProvider.showAlert("Lỗi mạng, vui lòng thử lại sau!")
    })
  }

  initSlide() {
    if(this.slides) {
      this.slides.slidesPerView = 4;
      this.slides.spaceBetween = 15;
      this.slides.centeredSlides = true;
      this.slides.freeMode = true;
      this.slides.initialSlide = 0;
    }
  }

  removeImage(event, name) {
    event.stopPropagation();
    event.preventDefault();
    let alert = this.alertController.create({
      title: 'THÔNG BÁO',
      message: 'Bạn có muốn xoá mã này không?',
      buttons: [
        {
          text: 'Đồng ý',
          handler: () => {
            this.doRemove(name);
          }
        },
        {
          text: 'Hủy',
          handler: () => {
          }
        }
      ]
    });
    alert.present();
  }

  doRemove(name) {
    this.restProvider.POST('', {name: name}, this.removeUrl, null, true).subscribe((res) => {
      if(res['success']) {
        this.getListQR();
      } else {
        this.restProvider.showAlert(res['message']);
      }
    }, err => {
      this.restProvider.showAlert(err.message);
    })
  }
}
