import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { RestProvider } from '../../providers/rest/rest';
import * as moment from 'moment';

/**
 * Generated class for the ListPromoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-list-promo',
  templateUrl: 'list-promo.html',
})
export class ListPromoPage {

  private urlPromo = "https://pacificgo.asia/secret/promotional-transactions";
  public listPromo = [];
  public length: number;
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public restProvider: RestProvider
  ) {
    this.restProvider.showLoading();
    this.restProvider.GET('', null, this.urlPromo, false, true).subscribe(res => {
      this.restProvider.hideLoading();
      if(res && res.success) {
        this.listPromo = res.data.items;
        moment.locale('vi');
        this.listPromo.forEach((item, idx) => { 
          item.created_at && (this.listPromo[idx].created_atText = moment(item.created_at).format("HH giờ mm p\\hút ngày DD/MM/YYYY"));
        })
        this.length = res.data.totalCount;
      }
    }, err => {
      this.restProvider.hideLoading();
      this.restProvider.showAlert(err.message);
    })

  }

}
