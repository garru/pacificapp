import { Component } from '@angular/core';
import { NavController, NavParams, ModalController } from 'ionic-angular';
import { DetailHistoryPage } from '../detail-history/detail-history';
import { RestProvider } from '../../providers/rest/rest';
import * as moment from 'moment';
import { FilterPage } from '../filter/filter';
// import * as _ from 'lodash';
/**
 * Generated class for the HistorySharePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-history-transaction',
  templateUrl: 'history-transaction.html',
})
export class HistoryTransactionPage {
  public currentMonth = moment().month() + 1;
  public currentYear = moment().year();
  private currentTime = moment().format("YYYY-MM-DD");
  public urlTransactions = "https://pacificgo.asia/secret/transactions";
  public listTransaction: any = null;
  public userId: number;
  public length: any;
  public userRoleId: number;
  public listTransactionCurrent: any;
  public listTransactionFilter: any;
  public onSearch = false;
  public onFilter = false;
  public searchText = "";
  public dataFilter = {
    monthYear: this.currentTime
  };
  // private currentPage = 0;
  // private totalPage;
  // private totalTransaction;
  // private quantityTransaction;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private restProvider: RestProvider,
    public modalController: ModalController
  ) {
    this.userId = this.restProvider.getUserInforItem('_id')
    this.userRoleId = this.restProvider.getUserInforItem('userRoleId');
    // this.urlTransactions += this.userId + '/transactions';
    this.getListTransition();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HistoryPage');
  }

  gotoHistoryDetai(item) {
    this.navCtrl.push(DetailHistoryPage, { data: item, userRoleId: this.userRoleId });
  }

  search() {
    this.listTransactionCurrent = [];
    this.listTransactionFilter.forEach(item => {
      let temp = JSON.stringify(item);
      (temp.toLowerCase().indexOf(this.searchText.toLowerCase()) !== -1) && this.listTransactionCurrent.push(item);
    });
  }

  filter() {
    let filterPage = this.modalController.create(FilterPage, { dataFilter: this.dataFilter });
    filterPage.present();
    filterPage.onDidDismiss(data => {
      data && this.doFilter(data);
    })
  }

  clearSearch() {
    this.onSearch = false;
    this.searchText = "";
    this.listTransactionCurrent = this.listTransactionFilter;
  }
  toggleSearch() {
    this.onSearch = !this.onSearch;
    if (!this.onSearch) {
      this.clearSearch();
    }
  }

  doRefresh(referesher) {
    this.getListTransition(referesher);
  }

  getListTransition(referesher?) {
    this.restProvider.showLoading();
    let searchParam = this.generateFilterData();
    this.restProvider.GET('', null, this.urlTransactions + searchParam, false, true).subscribe(res => {
      this.restProvider.hideLoading();
      referesher && referesher.complete();
      if (res && res.data) {
        this.listTransaction = res.data.items;
        moment.locale('vi');
        this.listTransaction.forEach((item, idx) => {
          item.created_at && (this.listTransaction[idx].created_atText = moment(item.created_at).format("hh giờ mm p\\hút ss giây DD MMMM YYYY"));
          item.update_at && (this.listTransaction[idx].update_atText = moment(item.update_at).format("hh giờ mm p\\hút ss giây DD MMMM YYYY"));
          item.discount && (item.amountText = item.discount);
        })
        this.listTransactionCurrent = this.listTransaction;
        this.length = this.listTransactionCurrent.length;
        this.listTransactionFilter = this.listTransactionCurrent;
        // this.doFilter(this.dataFilter);
      } else {
        this.listTransaction = null;
        this.listTransactionCurrent = null;
        this.listTransactionFilter = null;
      }
    }, err => {
      this.restProvider.hideLoading();
      referesher && referesher.complete();
      this.restProvider.showAlert(err.message);
    })
  }

  doFilter(data) {
    this.dataFilter = data;
    this.getListTransition();
    // if (!_.isEmpty(data)) {
    //   this.listTransactionFilter = [];
    //   data.month && (this.currentMonth = data.month + 1);
    //   data.year && (this.currentYear = data.year);
    //   this.listTransaction.forEach(item => {
    //     let perfect = true;
    //     _.forEach(data, (v, k) => {
    //       switch (k) {
    //         case "startDate":
    //           if (v && moment(item.created_at).valueOf() <= moment(v).valueOf()) {
    //             perfect = false;
    //           }
    //           break;
    //         case "endDate":
    //           if (v && moment(item.created_at).valueOf() >= moment(v).valueOf()) {
    //             perfect = false;
    //           }
    //           break;
    //         case "min":
    //           if (v && item.amount < parseInt(v)) {
    //             perfect = false;
    //           }
    //           break;
    //         case "max":
    //           if (v && item.amount > parseInt(v)) {
    //             perfect = false;
    //           }
    //           break;
    //         case "month":
    //           if (v && v !== moment(item.created_at).month()) {
    //             perfect = false;
    //           }
    //           break;
    //         case "year":
    //           if (v && v !== moment(item.created_at).year()) {
    //             perfect = false;
    //           }
    //           break;
    //       }
    //     })
    //     perfect && this.listTransactionFilter.push(item);
    //   })
    // this.length =  this.listTransactionFilter.length;
    this.clearSearch();
    // } else {
    //   this.listTransactionFilter = this.listTransaction;
    //   this.length =  this.listTransactionFilter.length;
    //   this.clearSearch();
    // }
  }

  generateFilterData() {
    let time = moment(this.dataFilter.monthYear).format();
    const startOfMonth = moment(time, "YYYY-MM-DD").utc(true).startOf('month').unix();
    const endOfMonth = moment(time).endOf('month').utc(true).unix();
    return '?' + 'from=' + startOfMonth + '&to=' + endOfMonth + '&owner=true'; 
  }

}