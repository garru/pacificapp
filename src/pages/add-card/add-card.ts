import { Component, ApplicationRef } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import * as moment from 'moment';
import { AddNewCardPage } from '../add-new-card/add-new-card';

/**
 * Generated class for the AddCardPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-add-card',
  templateUrl: 'add-card.html',
})
export class AddCardPage {
  public wallet: FormGroup;
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private formBuilder: FormBuilder,
    private elementRef: ApplicationRef
    ) {
      let now = moment().format("YYYY-MM-DD");
      this.wallet = this.formBuilder.group({
        cardNumber: ['1234 5678 1234 5678', [Validators.required]],
        expiryDate: [now, [Validators.required]],
        cvv: ['123', [Validators.required]],
        name: ['Bùi Thị Thịnh', [Validators.required]]
      })
      
      this.wallet.controls.expiryDate.valueChanges.subscribe(value => {
        
      })

      this.elementRef.tick();
  }

  ionViewDidLoad() {
    
  }

  gotoAddcard() {
    this.navCtrl.push(AddNewCardPage);
  }

}
