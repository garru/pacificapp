import { Component } from '@angular/core';
import { NavController, NavParams, MenuController, AlertController } from 'ionic-angular';
import { RestProvider } from '../../providers/rest/rest';
// import { ListPage } from '../list/list';
import { UserinformationPage } from '../userinformation/userinformation';
import { HistoryPage } from '../history/history';
import { InformationPage } from '../information/information';
import { LoginPage } from '../login/login';
import { ChangePasswordPage } from '../change-password/change-password';
import { POSITION, KEY_STORAGE } from '../../share/constant';
import { PromoPage } from '../promo/promo';
import { ListPromoPage } from '../list-promo/list-promo';
import { Events } from 'ionic-angular';
/**
 * Generated class for the MenuPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-menu',
  templateUrl: 'menu.html',
})
export class MenuPage {
  public userInfor = {};
  public imgLetters = '';
  public pages: Array<{ title: string, component: any }>;
  public positon = POSITION;
  // private urlLogout = '/api/v1/logout';
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public restProvider: RestProvider,
    public menuCtrl: MenuController,
    public alertController: AlertController,
    public events: Events
  ) {
    this.userInfor = this.restProvider.getUserInfor();
    this.imgLetters = !this.userInfor['avatar'] ? this.userInfor['name'][0] : '';

     if(this.userInfor['userRoleId'] === 1 || this.userInfor['userRoleId'] === 2) {
      this.pages = [
        { title: 'Thông tin tài khoản', component: UserinformationPage },
        { title: 'Lịch sử giao dịch', component: HistoryPage },
        { title: 'Thông tin ứng dụng', component: InformationPage }
      ];
     } else if (this.userInfor['userRoleId'] === 3) {
      this.pages = [
        { title: 'Thông tin tài khoản', component: UserinformationPage },
        { title: 'Ưu đãi', component: PromoPage },
        { title: 'Lịch sử giao dịch', component: HistoryPage },
        { title: 'Thông tin ứng dụng', component: InformationPage },
        { title: 'Lịch sử đổi quà', component: ListPromoPage}
      ];
     } else {
      this.pages = [
        { title: 'Thông tin tài khoản', component: UserinformationPage },
        { title: 'Lịch sử giao dịch', component: HistoryPage },
        { title: 'Thông tin ứng dụng', component: InformationPage }
      ];
     }
    
    events.subscribe('logout', () => {
      this.restProvider.removeLocalStorage('TOKEN');
      this.restProvider.removeLocalStorage('USER_INFOR');
      this.restProvider.removeLocalStorage(KEY_STORAGE.LOGIN_TIME);
      this.navCtrl.setRoot(LoginPage);
      this.restProvider.showAlert('Token hết hạn, vui lòng đăng nhập lại!');
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    // this.restProvider.fakeLoading();
    this.navCtrl.push(page.component);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MenuPage');
  }

  enableSwipe() {
    this.menuCtrl.swipeEnable(true);
  }

  disableSwipe() {
    this.menuCtrl.swipeEnable(false);
  }

  logOut() {
    let alert = this.alertController.create({
      title: 'ĐĂNG XUẤT',
      message: 'Bạn có muốn đăng xuất?',
      buttons: [
        {
          text: 'Đồng ý',
          handler: () => {
            this.restProvider.fakeLoading();
            this.restProvider.removeLocalStorage('TOKEN');
            this.restProvider.removeLocalStorage('USER_INFOR');
            this.navCtrl.setRoot(LoginPage);
          }
        },
        {
          text: 'Hủy',
          handler: () => {
          }
        }
      ]
    });
    alert.present();
  }

  changePassword() {
    this.restProvider.fakeLoading();
    this.navCtrl.push(ChangePasswordPage);
  }


}
