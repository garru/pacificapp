import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { RestProvider } from '../../providers/rest/rest';
import * as moment from 'moment';

/**
 * Generated class for the WithdrawalDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-withdrawal-detail',
  templateUrl: 'withdrawal-detail.html',
})
export class WithdrawalDetailPage {
  public detailData : any;
  public userRoleId: any;
  public salerName : any;
  public isPay: any;
  public isMember: any;
  // private getUserInforUrl = 'secret/user/'

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public rest: RestProvider
  ) {
    this.detailData = this.navParams.get('data') || {};
    this.isPay = this.navParams.get('isPay');
    this.isMember = this.navParams.get('isMember');
    this.userRoleId = this.navParams.get('userRoleId');
    this.detailData.bankaccount &&  (this.detailData.bankaccount = this.rest.encodeBankAccount(this.detailData.bankaccount));
    this.detailData.totalAmountText = Math.abs(this.detailData.amount).toLocaleString('it-IT', {style : 'currency', currency : 'VND'})
    this.detailData.created_atText = 
    moment(moment(this.detailData.created_at).format()).format("HH giờ mm p\\hút ngày DD/MM/YYYY");
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DetailHistoryPage');
  }

}
