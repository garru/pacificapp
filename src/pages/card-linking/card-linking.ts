import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { AddCardPage } from '../add-card/add-card';

/**
 * Generated class for the CardLinkingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-card-linking',
  templateUrl: 'card-linking.html',
})
export class CardLinkingPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CardLinkingPage');
  }

  gotoAddcard() {
    this.navCtrl.push(AddCardPage);
  }

}
