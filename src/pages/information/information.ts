import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { RestProvider } from '../../providers/rest/rest';

/**
 * Generated class for the InformationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-information',
  templateUrl: 'information.html',
})
export class InformationPage {
  private urlInfo = "/secret/info-app";
  public infoApp = [];
  constructor(public navCtrl: NavController, public navParams: NavParams, public restProvider: RestProvider) {
    this.restProvider.GET(this.urlInfo, null, 'https://pacificgo.asia', null, true).subscribe(res => {
      this.restProvider.hideLoading();
      if(res && res.success) {
        this.infoApp = res.data.items;
      }
    }, err => {
      this.restProvider.hideLoading();
      this.restProvider.showAlert(err.message);
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad InformationPage');
  }

}
