import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { RestProvider } from '../../providers/rest/rest';
import * as moment from 'moment';
import { PayPromoPage } from '../pay-promo/pay-promo';

/**
 * Generated class for the PromoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-promo',
  templateUrl: 'promo.html',
})
export class PromoPage {
  private urlPromo = "https://pacificgo.asia/secret/promotions";
  public listPromo = [];
  public length: number;
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public restProvider: RestProvider
  ) {
    this.restProvider.GET('', null, this.urlPromo, null, true).subscribe(res => {
      this.restProvider.hideLoading();
      if(res && res.success) {
        this.listPromo = res.data.items;
        moment.locale('vi');
        this.listPromo.forEach((item, idx) => {
          item.created_at && (this.listPromo[idx].created_at = moment(item.created_at).format("DD MMMM YYYY"));
        })
        this.length = res.data.totalCount;
      }
    }, err => {
      this.restProvider.hideLoading();
      this.restProvider.showAlert(err.message);
    })

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PromoPage');
  }

  gotoPayPromo(item) {
    this.navCtrl.push(PayPromoPage, {data: item});
  }

}
