import { Component, ApplicationRef } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { CardLinkingPage } from '../card-linking/card-linking';
import { RestProvider } from '../../providers/rest/rest';
import { WithdrawalPage } from '../withdrawal/withdrawal';
import { WithdrawalRequestPage } from '../withdrawal-request/withdrawal-request';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser';
import { SpinnerDialog } from '@ionic-native/spinner-dialog';

/**
 * Generated class for the WalletPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-wallet',
  templateUrl: 'wallet.html',
})
export class WalletPage {
  public userInfor: any;
  public userRoleId: number;
  private userId: number;
  private token;
  private getUserUrl = 'https://pacificgo.asia/secret/users/'
  private getWithdrawalHistoryUrl = 'https://pacificgo.asia/secret/vtcpay/history?partner_id=';
  private withdrawalUrl = 'https://pacificgo.asia/withdrawal';
  public discount;
  public discountPoint;
  public historyCount = 0;
  public withDrawal = [];
  public discountList = [];
  private browser;

  options: InAppBrowserOptions = {
    location: 'yes',//Hien location bar
    hidden: 'no', //khong clear cookie
    clearcache: 'yes',
    clearsessioncache: 'yes',
    hardwareback: 'no',
    mediaPlaybackRequiresUserAction: 'no',
    shouldPauseOnSuspend: 'no', //Android only 
    closebuttoncaption: 'Trở về App', //iOS only
    hidenavigationbuttons: "yes",
    hideurlbar: "yes",
    closebuttoncolor: "red",
    // toolbarcolor: "transparent",
    disallowoverscroll: 'no', //iOS only 
    toolbar: 'yes', //iOS only 
    enableViewportScale: 'no', //iOS only 
    allowInlineMediaPlayback: 'no',//iOS only 
    presentationstyle: 'pagesheet',//iOS only 
    beforeload: 'yes',
    zoom: 'no'
  };
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private restProvider: RestProvider,
    private iab: InAppBrowser,
    private elementRef: ApplicationRef,
    private spinnerDialog: SpinnerDialog
  ) {

  }

  ionViewWillEnter() {
    this.getInfor();
  }

  gotoCardLink(isGiftCard?) {
    this.navCtrl.push(CardLinkingPage, { isGiftCard: isGiftCard });
  }

  gotoWithDrawal(isPay, isMember) {
    this.navCtrl.push(WithdrawalPage, { isPay: isPay, isMember: isMember });
  }

  gotoWithDrawalRequest() {
    this.navCtrl.push(WithdrawalRequestPage);
  }

  public openWithDrawal() {
    let target = "_blank";
    this.browser = this.iab.create(this.withdrawalUrl, target, this.options);
    this.browser.on('loadstart').subscribe(event => {
      this.spinnerDialog.show("Đang chuyển hướng ...", "Xin quý khách đợi trong giây lát!", this.closeBrowser)
    })
    this.browser.on('loadstop').subscribe(event => {
      this.spinnerDialog.hide();
      this.browser.insertCSS({ code: "app-header,app-footer{display: none}" }); // hidden header and footer
      this.browser.executeScript({ code: `localStorage.setItem('userIdWithdrawal', '${JSON.stringify(this.userInfor)}')` }); // send userId
      this.browser.executeScript({ code: `localStorage.setItem('tokenId', '${this.token}')` }); // send Token_id
    })

    this.browser.on('loaderror').subscribe(event => {
      this.spinnerDialog.hide();
      this.restProvider.showAlert("Tạm thời không truy cập được cổng thanh toán. Vui lòng thử lại sau");
    })

    this.browser.on('exit').subscribe(res => {
      this.spinnerDialog.hide();
      this.getInfor();
    })
  }

  closeBrowser() {
    this.browser && this.browser.close();
  }

  getInfor() {
    this.userInfor = this.restProvider.getUserInfor();
    this.userRoleId = this.userInfor.userRoleId;
    this.userId = this.restProvider.getUserInforItem('_id');
    this.token = this.restProvider.getLocalStorage('TOKEN');
    this.restProvider.GET('', null, this.getUserUrl + this.userId, null, true).subscribe(res => {
      if (res && res.data && res.data.User) {
        this.userInfor = res.data.User;
        this.restProvider.setUser(res);
        (this.userRoleId === 3) && (this.discount = this.userInfor.discount);
        (this.userRoleId !== 3) && (this.discount = this.restProvider.formatCurrency(this.userInfor.amount || 0));
        this.restProvider.GET('', null, this.getWithdrawalHistoryUrl + this.userId, true, true).subscribe(res => {
          if (res && res.data && res.data) {
            this.historyCount = res.data.totalCount || 0;
            (this.userRoleId === 2) && this.formatWallet(res.data);
            this.elementRef.tick();
          } else {
            this.restProvider.showAlert(res.message);
          }
        }, err => {
          this.restProvider.showAlert(err.message);
        });
      } else {
        this.restProvider.showAlert(res.message);
      }
    }, err => {
      this.restProvider.showAlert(err.message);
    });
  }

  formatWallet(data) {
    this.withDrawal = [];
    this.discountList = [];
    data.items.forEach((item) => {
      if(item.status === 1) {
        this.withDrawal.push(item);
      } else {
        this.discountList.push(item);
      }
    })
  }

}
