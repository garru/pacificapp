import { Component, ApplicationRef } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { SuccessPage } from '../success/success';
import { RestProvider } from '../../providers/rest/rest';
import { SALE_OFF } from '../../share/constant';

/**
 * Generated class for the PaymentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-payment',
  templateUrl: 'payment.html',
})
export class PaymentPage {

  public detailData: any;
  public money: any = '';
  public note = 'Khách hàng sẽ được chiết khấu 70% trong số tiền giảm';
  public error = '';
  public userId: any;
  public userInfor;
  public totalMoney;
  private urlCheckout = 'https://pacificgo.asia/secret/checkout';
  private urlSearchUser = 'secret/users?search=';
  public isCheck = false;
  private minSale;
  private maxSale;
  public saleOptions = [];
  public sale = 0;
  public hasId: boolean = true;
  public newId;
  private timer;
  public listCustomer = [];
  public customer_id;
  public introducer_id;
  public notHasId;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public restProvider: RestProvider,
    public applicationRef: ApplicationRef
  ) {
    this.userInfor = this.restProvider.getUserInfor();
    this.userId = this.userInfor._id;
    this.detailData = this.navParams.get('data') || {};
    this.minSale = this.userInfor.minSale || 10;
    this.maxSale = this.userInfor.maxSale || 20;
    (!this.maxSale) && (this.maxSale = this.minSale);
    this.sale = this.maxSale;
    this.restProvider.hideLoading();
    for (let i = this.minSale; i <= this.maxSale; i++) {
      this.saleOptions.push({
        value: i,
        text: i
      })
    }
  }

  payment() {
    this.validateMoney();
    let moneyString = this.money.toString().replace(/\./g, '');
    this.hasId && (this.customer_id = this.userId);
    if (!moneyString || !this.customer_id || !this.sale) {
      this.error = "Hãy điền toàn bộ các trường!";
    } else {
      this.error = "";
    }
    if (this.error) {
    } else {
      if (this.isCheck) {
        let param = {
          member_id: this.detailData._id,
          amount: parseFloat(moneyString),
          commission: this.sale,
          customer_id: this.customer_id,
          location: '',
          desc: this.note,
          introducer_id: this.userInfor.introducer_id || 0
        }
        this.restProvider.showLoading();
        this.restProvider.POST('', param, this.urlCheckout, true, true).subscribe(res => {
          this.restProvider.hideLoading();
          if (res['success']) {
            this.navCtrl.setRoot(SuccessPage, { title: "THANH TOÁN THÀNH CÔNG", id: res['data']['Transaction'].id });
          } else {
            this.restProvider.showAlert(res['message']);
          }
        }, err => {
          this.restProvider.hideLoading();
          this.restProvider.showAlert(err.message);
        })
      } else {
        let money = parseFloat(moneyString);
        if (this.sale) {
          let saveMoney = money * this.sale / 100;
          let discountUser = saveMoney * SALE_OFF.USER;
          let discountIntroduce = this.userInfor.introducer_id ? saveMoney * SALE_OFF.USER_INTRODUCE : 0;
          let discountPacific = this.userInfor.introducer_id ? saveMoney * SALE_OFF.PACIFIC : saveMoney * SALE_OFF.PACIFIC_NO_INTRODUCE;
          this.totalMoney = Math.ceil(
            money - saveMoney +
            Math.ceil(discountUser || 0) +
            Math.ceil(discountIntroduce || 0) +
            Math.ceil(discountPacific || 0));
          this.totalMoney = this.restProvider.formatCurrency(this.totalMoney);
        } else {
          this.totalMoney = money;
          this.totalMoney = this.restProvider.formatCurrency(this.totalMoney);
        }
        this.isCheck = true;
        this.applicationRef.tick();
      }
    }
  }

  validateMoney() {
    this.money = parseInt(this.money.toString().replace(/\./g, ''));
    this.money = this.money ? this.restProvider.formatCurrency(this.money) : '';
    this.applicationRef.tick();
  }

  ionViewWillEnter() {
    this.hideCamera();
  }

  hideCamera() {
    (window.document.querySelector('ion-app') as HTMLElement).classList.remove('cameraView');
  }

  changeId(isHasId) {
    this.notHasId = !this.notHasId;
    this.hasId = !this.hasId;
    this.applicationRef.tick();
  }

  searchUser() {
    clearTimeout(this.timer);
    this.timer = setTimeout(() => {
      if (this.newId.length > 2) {
        this.restProvider.GET(`${this.urlSearchUser}${this.newId}&paginate=0&perPage=10`, null, null, null, true).subscribe(res => {
          if (res.data && res.data.items) {
            this.listCustomer = res.data.items.filter(item => item.userRoleId === 3);
          } else {
            this.restProvider.showAlert(res.message)
          }
        }, err => {
          this.restProvider.showAlert(err.message);
        })
      }
    }, 200)
  }

  resetListAutocomplete() {
    setTimeout(() => {
      this.listCustomer = [];
    }, 500)
  }

  selectProducer(user) {
    this.newId = user.username;
    this.customer_id = user._id;
    this.resetListAutocomplete();
  }

}
