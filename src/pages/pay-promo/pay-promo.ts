import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { RestProvider } from '../../providers/rest/rest';
import { Slides } from 'ionic-angular';
import * as _ from "lodash";
import { SuccessPage } from '../success/success';
/**
 * Generated class for the PayPromoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-pay-promo',
  templateUrl: 'pay-promo.html',
})
export class PayPromoPage {
  @ViewChild('slides') slides: Slides;
  public detailData = this.navParams.get('data');
  public userInfor = this.rest.getUserInfor();
  public discount = this.userInfor['discount'];
  public persionalInfor = {
    name: "",
    phone: "",
    address: ""
  };
  public error = {};
  public note = "";
  public urlBuyPromoItem = "https://pacificgo.asia/secret/promotions/" + this.detailData.id + "/purchase";
  public disabled = this.detailData.price > this.discount ? true : false;
  public quantity = 1;
  public total = this.detailData.price;
  public isUseDefault = false;
  public tempUserData: any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public rest: RestProvider
  ) {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PayPromoPage');
  }

  prevSlide() {
    this.slides.slideNext();
  }

  nextSlide() {
    this.slides.slidePrev();
  }

  payment() {
    let errorMess = false;
    if (!errorMess) {
      const desc = "Tên người nhận: " + (this.persionalInfor['name'] || "") + 
                    ". Số điện thoại: " + (this.persionalInfor['phone'] || "") + 
                    ". Địa chỉ nhận quà: " + this.persionalInfor['address'];
      this.rest.showLoading();
      this.rest.POST('', { description: desc, quantity: this.quantity }, this.urlBuyPromoItem, false, true).subscribe(res => {
        this.rest.hideLoading();
        if (res['success']) {
          this.navCtrl.setRoot(SuccessPage, { title: "Gởi yêu cầu đổi quà thành công. Vui lòng đợi xác nhận!" });
        } else {
          this.rest.showAlert("Đổi quà thất bại, vui lòng thử lại!");
        }
      }, err => {
        this.rest.hideLoading();
        this.rest.showAlert("Đổi quà thất bại, vui lòng thử lại!");
      })
    }
  }

  setValue() {
    if (this.isUseDefault) {
      this.tempUserData = this.persionalInfor;
      _.forEach(this.persionalInfor, (v, k) => {
        this.persionalInfor[k] = this.userInfor[k];
        this.error[k] = "";
      })
    } else {
      this.persionalInfor = this.tempUserData;
    }
  }

  calculatoPoint() {
    this.total = this.quantity * this.detailData.price;
    if (this.total > this.discount) {
      this.disabled = true;
    } else {
      this.disabled = false;
    }
  }

}
