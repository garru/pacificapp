import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ValidationService } from '../../share/validation';
import { SuccessPage } from '../success/success';
import { RestProvider } from '../../providers/rest/rest';


/**
 * Generated class for the ForgotPasswordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-forgot-password',
  templateUrl: 'forgot-password.html',
})
export class ForgotPasswordPage {

  public changePasswordForm: FormGroup;
  private url = "secret/users/resetPassword";

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private formBuilder: FormBuilder,
    private validationService: ValidationService,
    private rest: RestProvider
  ) {
    this.changePasswordForm = this.formBuilder.group({
      email: ['', [Validators.required, validationService.checkEmail]]
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChangePasswordPage');
  }

  submit() {
    this.validationService.validateAllFields(this.changePasswordForm);
    if (this.changePasswordForm.valid) {
      this.rest.showLoading();
      const param = this.changePasswordForm.value;
      this.rest.POST(this.url, param, '', true, true).subscribe(res => {
        this.rest.hideLoading();
        this.navCtrl.setRoot(SuccessPage, { title: "Mật khẩu mới đã được gởi về email. Vui lòng kiểm tra!" , islogin: true});
      }, err => {
        this.rest.hideLoading();
        this.rest.showAlert(err.message);
      })
    }
  }

}
