import { Component } from '@angular/core';
import { NavController, ActionSheetController, AlertController, Events } from 'ionic-angular';
import { QrcodePage } from '../qrcode/qrcode';
import { InformationPage } from '../information/information';
import { HistoryPage } from '../history/history';
import { RestProvider } from '../../providers/rest/rest';
import { MyQrCodePage } from '../my-qr-code/my-qr-code';
import { PromoPage } from '../promo/promo';
import { POSITION } from '../../share/constant';
import { UserinformationPage } from '../userinformation/userinformation';
import { LoginPage } from '../login/login';
import { ChangePasswordPage } from '../change-password/change-password';
import { WalletPage } from '../wallet/wallet';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  pages: Array<{ title: string, component: any, image?: string, showLoading?: boolean }>;
  public userRole: number;
  public userInfor: any;
  public name;
  public position: string;
  public discountPoint;
  public discount;
  private getUserUrl = 'https://pacificgo.asia/secret/users/';
  constructor(
    public navCtrl: NavController,
    private restProvider: RestProvider,
    public actionSheetController: ActionSheetController,
    private alertController: AlertController,
    private events: Events
  ) {
    this.events.subscribe('user:logout', () => {
      this.doLogout();
    });

  }

  ionViewWillEnter() {
    this.userInfor = this.restProvider.getUserInfor();
    this.restProvider.GET(
      '', null,
      this.getUserUrl + this.userInfor._id,
      null, true
    ).subscribe(res => {
      if (res && res.data && res.data.User) {
        this.userInfor = res.data.User;
        this.userRole = this.userInfor['userRoleId'];
        (this.userRole === 3) && (this.discount = this.userInfor.point);
        (this.userRole !== 3) && (this.discount = this.restProvider.formatCurrency(this.userInfor.amount || 0));
        this.name = this.userInfor.name;
        this.position = POSITION[this.userRole];
        this.setMenu();
        this.restProvider.setUser(res);
      } else {
        this.restProvider.showAlert(res.message);
      }
    }, err => {
      this.restProvider.showAlert(err.message);
    });

  };

  openPage(page) {
    page.showLoading && this.restProvider.showLoading();
    // if (page.title === 'Thanh toán' || page.title === 'List' || page.title === 'Thông tin') {
    //   this.restProvider.showLoading();
    // } else {
    //   if (page.title !== 'Lịch sử chis sẻ' && page.title !== 'Lịch sử liên kết' && page.title !== 'Mã của tôi' && page.title !== 'Lịch sử giao dịch') {
    //     this.restProvider.fakeLoading();
    //   }
    // }
    this.navCtrl.push(page.component);
  }

  openMenu() {
    const actionSheet = this.actionSheetController.create({
      cssClass: 'action-sheet--ios',
      buttons: [{
        text: 'Thông tin tài khoản',
        handler: () => {
          let page = { title: 'Thông tin tài khoản', component: UserinformationPage };
          this.openPage(page);
        }
      }, {
        text: 'Lịch sử giao dịch',
        handler: () => {
          let page = { title: 'Lịch sử giao dịch', component: HistoryPage };
          this.openPage(page);
        }
      }, {
        text: 'Thông tin ứng dụng',
        handler: () => {
          let page = { title: 'Thông tin ứng dụng', component: InformationPage };
          this.openPage(page);
        }
      }, {
        text: 'Đổi mật khẩu',
        handler: () => {
          let page = { title: 'Đổi mật khẩu', component: ChangePasswordPage };
          this.openPage(page);
        }
      }, {
        text: 'Đăng xuất',
        handler: () => {
          this.logOut();
        }
      }]
    });
    actionSheet.present();
  }

  logOut() {
    let alert = this.alertController.create({
      title: 'ĐĂNG XUẤT',
      message: 'Bạn có muốn đăng xuất?',
      buttons: [
        {
          text: 'Đồng ý',
          handler: () => {
            this.doLogout();
          }
        },
        {
          text: 'Hủy',
          handler: () => {
          }
        }
      ]
    });
    alert.present();
  }

  doLogout() {
    this.restProvider.fakeLoading();
    this.restProvider.removeLocalStorage('TOKEN');
    this.restProvider.removeLocalStorage('USER_INFOR');
    this.navCtrl.setRoot(LoginPage);
  }

  setMenu() {
    switch (this.userRole) {
      case 1:
        this.pages = [
          // { 
          //   title: 'Đăng ký thành viên', 
          //   component: RegisterPage,
          //   image: "../../assets/imgs/layout_green/icon-credit-card.png"
          // },
          {
            title: 'Lịch sử chis sẻ',
            component: HistoryPage,
            image: "../../assets/imgs/layout_green/icon-history.png"
          },
          {
            title: 'Thông tin',
            component: InformationPage,
            image: "../../assets/imgs/layout_green/icon-information.png",
            showLoading: true
          },

        ];
        break;
      case 2:
        this.pages = [
          {
            title: 'Thanh toán',
            component: QrcodePage,
            image: "../../assets/imgs/layout_green/ic_scan_qrcode.png",
            showLoading: true
          },
          {
            title: 'Lịch sử giao dịch',
            component: HistoryPage,
            image: "../../assets/imgs/layout_green/icon-history.png"
          },
          {
            title: 'Ví của tôi',
            component: WalletPage,
            image: "../../assets/imgs/layout_green/icon-credit-card.png",
            showLoading: false
          },
          {
            title: 'Thông tin',
            component: InformationPage,
            image: "../../assets/imgs/layout_green/icon-information.png",
            showLoading: true
          },
          // { 
          //   title: 'Register', 
          //   component: RegisterPage,
          //   image: "../../assets/imgs/register.png"
          // },
        ];
        break;
      case 3:
        this.pages = [
          {
            title: 'Lịch sử chis sẻ',
            component: HistoryPage,
            image: "../../assets/imgs/layout_green/icon-history.png"
          },
          {
            title: 'Khuyến mãi',
            component: PromoPage,
            image: "../../assets/imgs/layout_green/icon-coupon.png",
            showLoading: true
          },
          // {
          //   title: 'Ví của tôi',
          //   component: WalletPage,
          //   image: "../../assets/imgs/icon-credit-card.png",
          //   showLoading: false
          // },
          {
            title: 'Thông tin',
            component: InformationPage,
            image: "../../assets/imgs/layout_green/icon-information.png",
            showLoading: true
          },
          // { 
          //   title: 'Register', 
          //   component: RegisterPage,
          //   image: "../../assets/imgs/register.png"
          // },
          {
            title: 'Mã của tôi',
            component: MyQrCodePage,
            image: "../../assets/imgs/layout_green/icon-qr-code.png"
          },
        ];
        break;
      case 4:
        this.pages = [
          {
            title: 'Thu nhập của bạn',
            component: HistoryPage,
            image: "../../assets/imgs/layout_green/icon-history.png"
          },
          {
            title: 'Mã của tôi',
            component: MyQrCodePage,
            image: "../../assets/imgs/layout_green/icon-qr-code.png"
          },
          {
            title: 'Ví của tôi',
            component: WalletPage,
            image: "../../assets/imgs/layout_green/icon-credit-card.png",
            showLoading: false
          },
          {
            title: 'Thông tin',
            component: InformationPage,
            image: "../../assets/imgs/layout_green/icon-information.png",
            showLoading: true
          },
          // { 
          //   title: 'Register', 
          //   component: RegisterPage,
          //   image: "../../assets/imgs/register.png"
          // },

          // { 
          //   title: 'Lịch sử liên kết', 
          //   component: HistorySharePage,
          //   image: "../../assets/imgs/layout_green/icon-history.png"
          // },
          // { 
          //   title: 'Lịch sử giao dịch', 
          //   component: HistoryTransactionPage,
          //   image: "../../assets/imgs/layout_green/icon-history.png"
          // }
        ];
        break;
    }
  }

}
