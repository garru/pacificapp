import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';
import * as _ from 'lodash';
import * as moment from 'moment';

/**
 * Generated class for the FilterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-filter',
  templateUrl: 'filter.html',
})
export class FilterPage {
  public validate = {
    startDate: '',
    endDate: '',
    min: '',
    max: ''
  };
  public isEmpty = true;
  public isErr = false;
  public ngModal = this.navParams.get('dataFilter') || {};
  public futureDate = moment((moment().year() + 30).toString()).format("YYYY-MM-DD");
  public previousDate = moment((moment().year() - 30).toString()).format("YYYY-MM-DD");

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewController: ViewController
  ) {
    moment.locale('vi');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FilterPage');
  }

  resetFilter() {
    _.forEach(this.validate, (v, k) => {
      this.validate[k] = '';
      this.ngModal[k] = null;
    })
  }

  validateField(key) {
    this.isEmpty = false;
    if (this.ngModal[key]) {
      if (key === 'min' || key === 'max') {
        if (!this.ngModal[key].match(/^[0-9]*$/)) {
          this.validate[key] = "Chỉ được nhập số";
          this.isErr = true;
        } else {
          if (this.ngModal['min'] > this.ngModal['max']) {
            this.isErr = true;
            this.validate[key] = "Số tiền nhỏ nhất không được lớn hơn số tiền lớn nhất";
          } else {
            this.validate[key] = "";
            _.forEach(this.validate, (v, k) => {
              v && (this.isErr = true);
            })
          }
        }
      } else {
        if (this.ngModal['startDate'] && this.ngModal['endDate'] && (new Date(this.ngModal['startDate']).getTime() > new Date(this.ngModal['endDate']).getTime())) {
          this.isErr = true;
          this.validate[key] = "Thời gian bắt đầu phải nhỏ hơn thời gian kết thúc";
        } else {
          this.validate[key] = "";
          _.forEach(this.validate, (v, k) => {
            v && (this.isErr = true);
          })
        }
      }
    } else {
      this.validate[key] = "";
      _.forEach(this.validate, (v, k) => {
        v && (this.isErr = true);
      })
    }
  }

  filter() {
    this.viewController.dismiss(this.ngModal);
  }

}
