import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, AlertController, Navbar } from 'ionic-angular';

import { PaymentPage } from '../payment/payment';
import { RegisterPage } from '../register/register';
import { QRScanner, QRScannerStatus } from '@ionic-native/qr-scanner';
import { RestProvider } from '../../providers/rest/rest';
import * as JWT from 'jwt-decode';
import { isString, isObject } from 'ionic-angular/util/util';
/**
 * Generated class for the QrcodePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-qrcode',
  templateUrl: 'qrcode.html',
})
export class QrcodePage {
  @ViewChild(Navbar) navBar: Navbar;
  private scanSub;
  private urlRegister = '/secret/users';
  public isAccept = false;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public qrScanner: QRScanner,
    public restProvider: RestProvider,
    private alertController: AlertController
  ) {

  }

  gotoPayment() {
    this.navCtrl.push(PaymentPage);
  }

  gotoRegister() {
    this.navCtrl.setRoot(RegisterPage, { hideBackButton: true });
  }

  ionViewWillEnter() {
    setTimeout(() => {
      this.showCamera();
      this.qrScanner.prepare()
        .then((status: QRScannerStatus) => {
          if (status.authorized) {
            this.isAccept = true;
            this.startScan();
          } else if (status.denied) {
            this.restProvider.showAlert('Vui lòng cho phép truy cập Camera để tiếp tục');
          } else {
            this.restProvider.showAlert('Vui lòng cho phép truy cập Camera để tiếp tục');
          }
        })
        .catch((e: any) => this.restProvider.showAlert(e.message || e._message));
    }, 500)

    setTimeout(() => {
      this.restProvider.hideLoading();
    }, 1000)

    this.navBar.backButtonClick = () => {
      setTimeout(() => {
        this.navCtrl.pop();
      }, 700)
      this.restProvider.showLoading();
      setTimeout(() => {
        this.restProvider.hideLoading();
      }, 1000)
    }
  }

  ionViewWillLeave() {
    this.hideCamera();
    this.qrScanner.hide();
    if (this.scanSub) {
      this.scanSub.unsubscribe();
    }
  }
  showCamera() {
    (window.document.querySelector('ion-app') as HTMLElement).classList.add('cameraView');
  }

  hideCamera() {
    (window.document.querySelector('ion-app') as HTMLElement).classList.remove('cameraView');
  }

  startScan() {
    this.scanSub = this.qrScanner.scan().subscribe((data: string) => {
      if (data) {
        if (this.restProvider.isJSONString(data) && JSON.parse(data)._id) {
          this.scanQrCode(JSON.parse(data));
        } else {
          try {
            let name = JWT(data);
            (isObject(name) && name.result) && (name = name.result);
            if (name && isString(name)) {
              this.restProvider.showLoading();
              this.restProvider.GET('/api/Get_InfoUser?username=' + name, null, 'https://pacificlink.asia').subscribe(res => {
                if (res && res.check) {
                  const params = {
                    address: res.data.Address,
                    email: res.data.Email,
                    gender: res.data.Gender || 0,
                    name: res.data.FullName || res.data.BankAccountHolder,
                    username: res.data.UserName,
                    phone: res.data.PhoneNumber || null,
                    birthday: res.data.Birthday,
                    password: res.data.UserName,
                    saleOff: 0,
                    introducer_id: null,
                    otp_validate: 1,
                    userRoleId: 4
                  }
                  this.restProvider.POST(this.urlRegister, params, 'https://pacificgo.asia', true, true).subscribe(res => {
                    this.restProvider.hideLoading();
                    
                    if ((res && res['data'] && res['data']['User'] && res['data']['User'][0]) || (res['user'] && res['user'][0])) {
                      if (res['data'] && res['data']['User'] && res['data']['User'][0]) {
                        this.scanQrCode(res['data']['User'][0]);
                      } else {
                        this.scanQrCode(res['user'][0]);
                      }
                    } else {
                      this.restProvider.hideLoading();
                      this.alertRegister();
                      // this.restProvider.showAlert("Mã chưa được đăng ký");
                    }
                  }, err => {
                    this.restProvider.hideLoading();
                    this.restProvider.showAlert(err.message);
                  })
                } else {
                  this.restProvider.hideLoading();
                  this.alertRegister();
                  // this.restProvider.showAlert("Mã chưa được đăng ký");
                }
              }, err => {
                this.restProvider.hideLoading();
                this.alertRegister();
                // this.restProvider.showAlert("Mã chưa được đăng ký");
              })
            } else {
              this.alertRegister();
              // this.restProvider.showAlert("Mã chưa được đăng ký");
            }
            // valid token format
          } catch (error) {
            this.alertRegister();
            // this.restProvider.showAlert("Mã chưa được đăng ký");
          }

        }


        // if (JWT(data)) {

        // }
      } else {
        this.alertRegister();
        // this.restProvider.showAlert("Mã chưa được đăng ký");
      }
    });
    this.qrScanner.show();
  }

  alertRegister() {
    let alert = this.alertController.create({
      message: "Mã chưa được đăng ký",
      buttons: [

        {
          text: 'Quét lại',
          handler: () => {
            this.startScan();
          }
        }
      ]
    });
    alert.present();
  }

  scanQrCode(data) {
    this.restProvider.showLoading();
    this.qrScanner.hide();
    this.scanSub.unsubscribe();
    // this.hideCamera();
    setTimeout(() => {
      this.navCtrl.push(PaymentPage, { data: data });
    }, 1000)
  }
}
