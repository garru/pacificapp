import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Navbar } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ValidationService } from '../../share/validation';
import { RestProvider } from '../../providers/rest/rest';
import { OtpPage } from '../otp/otp';
import { HomePage } from '../home/home';
import * as moment from 'moment';
import { USEROLE_ID } from '../../share/constant';

/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-register',
  templateUrl: 'register.html',

})
export class RegisterPage {
  @ViewChild(Navbar) navBar: Navbar;
  public registerForm: FormGroup;
  private urlRegister = 'admin/api/v1/signUp';
  public confirmPass = "";
  public hideBackButton = false;
  public showPass = {
    pass: false,
    confirm: false
  }
  public role = USEROLE_ID;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private formBuilder: FormBuilder,
    public validationService: ValidationService,
    private restProvider: RestProvider
  ) {
    this.registerForm = this.formBuilder.group({
      username: ['', [Validators.required, Validators.minLength(8), this.validationService.checkUsername]],
      phone: ['', [Validators.required, Validators.minLength(10)]],
      password: ['', [Validators.required, Validators.minLength(8)]],
      confirmPassword: [''],
      gender: [0, Validators.required],
      userRoleId: [3, Validators.required],
      name: ['', [Validators.required, Validators.minLength(8), this.validationService.checkName]],
      address: ['', [Validators.required, Validators.minLength(8)]],
      dayOfBird: ['', Validators.required],
      monthOfBird: ['', Validators.required],
      yearOfBird: ['', Validators.required],
      email: ['', [this.validationService.checkEmail, Validators.required]]
    })
    this.hideBackButton = this.navParams.get('hideBackButton');
  }

  ionViewDidLoad() {
    this.hideBackButton && this.setBackButtonAction();
  }

  setBackButtonAction() {
    this.navBar.backButtonClick = () => {
      this.navCtrl.setRoot(HomePage);
    }
  }

  submit() {
    if (this.registerForm.valid && !this.confirmPass) {
      let birthday = `${this.registerForm.controls.monthOfBird.value}-${this.registerForm.controls.dayOfBird.value}-${this.registerForm.controls.yearOfBird.value}`
      let param = {
        username: this.registerForm.controls.username.value,
        phone: this.registerForm.controls.phone.value,
        password: this.registerForm.controls.password.value,
        gender: parseInt(this.registerForm.controls.gender.value),
        name: this.registerForm.controls.name.value,
        address: this.registerForm.controls.address.value,
        birthday: moment(birthday).valueOf(),
        saleOff: 0,
        email: this.registerForm.controls.email.value,
        userRoleId: 3
      }
      this.restProvider.showLoading();
      this.restProvider.POST(this.urlRegister, param, '', true, true).subscribe(res => {
        this.restProvider.hideLoading();
        if (res && res['data']) {
          if (res['success']) {
            let userId = res['data']['User']['_id'];
            let phone = res['data']['User']['phone'];
            this.restProvider.fakeLoading();
            this.navCtrl.push(OtpPage, {userId: userId, phone: phone});
          } else {
            this.restProvider.showAlert(res['message']);
          }
        } else {
          this.restProvider.showAlert(res['message']);
        }
      }, err => {
        this.restProvider.showAlert(err.message);
        this.restProvider.hideLoading();
      })
    } else {
      this.validationService.validateAllFields(this.registerForm);
      this.validatePassword();
    }
  }

  validatePassword() {
    if(this.registerForm.controls.confirmPassword.value) {
      if (this.registerForm.controls.confirmPassword.value !== this.registerForm.controls.password.value) {
        this.confirmPass = "Xác nhận mật khẩu không chính xác.";
      } else {
        this.confirmPass = "";
      }
    } else {
      this.confirmPass = "Trường bắt buộc."
    }
  }

  displayPassword(key) {
    this.showPass[key] = !this.showPass[key];
  }
}
