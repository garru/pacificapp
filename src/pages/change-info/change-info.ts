import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Navbar } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ValidationService } from '../../share/validation';
import { RestProvider } from '../../providers/rest/rest';
import { HomePage } from '../home/home';
import * as moment from 'moment';
import { USEROLE_ID } from '../../share/constant';

/**
 * Generated class for the ChangeInfoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-change-info',
  templateUrl: 'change-info.html',
})
export class ChangeInfoPage {

  @ViewChild(Navbar) navBar: Navbar;
  public registerForm: FormGroup;
  private urlRegister = 'https://pacificgo.asia/secret/users/';
  public hideBackButton = false;
  private userInfo = this.navParams.get('userInfo');

  public role = USEROLE_ID;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private formBuilder: FormBuilder,
    public validationService: ValidationService,
    private restProvider: RestProvider
  ) {
    let dayOfBird, monthOfBird, yearOfBird;
    if(this.userInfo.birthday) {
      let time = moment(this.userInfo.birthday).format("MM-DD-YYYY");
      let timeArr = time.split('-');
      dayOfBird = timeArr[0];
      monthOfBird = timeArr[1];
      yearOfBird = timeArr[2];
    } else {
      dayOfBird = '';
      monthOfBird = '';
      yearOfBird = '';
    }
    this.registerForm = this.formBuilder.group({
      username: [this.userInfo.username],
      phone: [this.userInfo.phone, [Validators.required, Validators.minLength(10)]],
      gender: [this.userInfo.gender, Validators.required],
      name: [this.userInfo.name, [Validators.required, Validators.minLength(8), this.validationService.checkName]],
      address: [this.userInfo.address, [Validators.required, Validators.minLength(8)]],
      dayOfBird: [dayOfBird, Validators.required],
      monthOfBird: [monthOfBird, Validators.required],
      yearOfBird: [yearOfBird, Validators.required],
      email: [this.userInfo.email, [this.validationService.checkEmail, Validators.required]]
    })
  }

  ionViewDidLoad() {
    this.hideBackButton && this.setBackButtonAction();
  }

  setBackButtonAction() {
    this.navBar.backButtonClick = () => {
      this.navCtrl.setRoot(HomePage);
    }
  }

  submit() {
    if (this.registerForm.valid) {
      let birthday = `${this.registerForm.controls.monthOfBird.value}-${this.registerForm.controls.dayOfBird.value}-${this.registerForm.controls.yearOfBird.value}`
      let param = {
        username: this.registerForm.controls.username.value,
        phone: this.registerForm.controls.phone.value,
        gender: parseInt(this.registerForm.controls.gender.value),
        name: this.registerForm.controls.name.value,
        address: this.registerForm.controls.address.value,
        birthday: moment(birthday).valueOf(),
        email: this.registerForm.controls.email.value,
        userRoleId: this.userInfo.userRoleId
      }
      this.restProvider.showLoading();
      this.restProvider.PUT('', param, this.urlRegister + this.userInfo._id, true, true).subscribe(res => {
        this.restProvider.hideLoading();
        if (res && res['data']) {
          if (res['success']) {
            this.navCtrl.pop();
          } else {
            this.restProvider.showAlert(res['message']);
          }
        } else {
          this.restProvider.showAlert(res['message']);
        }
      }, err => {
        this.restProvider.showAlert(err.message);
        this.restProvider.hideLoading();
      })
    } else {
      this.validationService.validateAllFields(this.registerForm);
    }
  }
}
