import { Component, Input } from '@angular/core';
import { ValidationService } from './validation';
import { FormControl } from '@angular/forms';

/*
  Generated class for the ValidationMessageProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Component({
  selector: 'control-messages',
  template: `<p class="error-message" *ngIf="errorMessage !== null">{{errorMessage}}</p>`
})
export class ValidationControlMessages {

  @Input() control: FormControl;

  constructor(private validationService: ValidationService) { }

  get errorMessage() {
    if (this.control) {
      for (let propertyName in this.control.errors) {
        if (this.control.errors.hasOwnProperty(propertyName) && this.control.touched) {
          return this.validationService.getValidatorErrorMessage(propertyName, this.control.errors[propertyName]);
        }
      }
      return null;
    }
  }

}
