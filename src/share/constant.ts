export const USER_ROLE = {
  "ADMIN": 1,
  "PARTNER": 2,
  "USER" : 3,
  "MEMBER": 4
}

export const POSITION = {
  1: 'Admin',
  2: 'Đối tác',
  3: 'Khách hàng',
  4: 'Thành viên'
}

export const SALE_OFF = {
  USER_INTRODUCE: 10/100, // 10%
  PACIFIC: 10/100, // 10%
  PACIFIC_NO_INTRODUCE: 20/100, //20%,
  USER: 10/100 // 10%
}

export const KEY_STORAGE = {
  TOKEN: "pacific_token",
  USER_INFOR: "user_infor",
  LOGIN_TIME: "login_time",
  EXPIRY_TIME: "expiry_time"
}
export const USEROLE_ID = [
  {
    code: 1,
    text: "Admin"
  },
  {
    code: 2,
    text: "Đối tác"
  },
  {
    code: 3,
    text: "Khách hàng"
  },
  {
    code: 4,
    text: "Thành viên"
  }
]