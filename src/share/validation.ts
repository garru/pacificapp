import { Injectable } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
/*
  Generated class for the ValidationProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ValidationService {


  getValidatorErrorMessage(validatorName: string, validatorValue?: any) {
    let config = {
      'required': 'Trường bắt buộc',
      'minlength': 'Quá ngắn. Trường phải có ít nhất ' + validatorValue.requiredLength + ' kí tự.',
      'invalidConfirmPassword': 'Xác nhận mật khẩu không chính xác.',
      'invalidOnlyNumber': 'Chỉ chấp nhận số.',
      'invalidEmail': 'Email sai định dạng.',
      'invalidName': 'Tên không hợp lệ.',
      'invalidUsername': 'Tên tài khoản không hợp lệ.',
    };

    return config[validatorName];
  }

  minlength(control) {
    if (control.value && control.value.length || !control.value) {
      return null;
    } else {
      return { 'minlength': true }
    }
  }

  // passwordValidator(control) {
  //   if (control.value.match(/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*()_+|~=`{}\[\]:";'<>?,./-]).{9,}$/g)) {
  //     return null;
  //   } else {
  //     return { 'invalidPassword': true };
  //   }
  // }

  onlyNumber(control) {
    if (control.value && control.value.match(/^[0-9]*$/) || !control.value) {
      return null;
    } else {
      return { 'invalidOnlyNumber': true }
    }
  }

  checkEmail(control) {
    if (control.value && control.value.match(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/g) || !control.value) {
      return null;
    } else {
      return { 'invalidEmail': true };
    }
  }

  confirmPassword(myForm) {
    if(myForm && myForm.value) {
      if (myForm.value.password === myForm.value.confirmPassword) {
        myForm.controls.confirmPassword.setErrors(null);
        return null
      } else {
        myForm.controls.confirmPassword.setErrors({ "incorrect": true });
        return { 'invalidConfirmPassword': true };
      }
    } else {
      return null;
    }
  }

  checkName(control) {
    if (control.value && control.value.match(/^[a-zA-Z0-9 _ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀẾỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểếỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ\\s]+$/g) || !control.value) {
      return null;
    } else {
      return { 'invalidName': true };
    }
  }

  checkUsername(control) {
    if (control.value && control.value.match(/^[A-Za-z0-9]+(?:[. _-][A-Za-z0-9]+)*$/g) || !control.value) {
      return null;
    } else {
      return { 'invalidUsername': true };
    }
  }

  validateAllFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFields(control);
      }
    });
  }

}
