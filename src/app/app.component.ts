import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { HomePage } from '../pages/home/home';
import { RestProvider } from '../providers/rest/rest';
import { LoginPage } from '../pages/login/login';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  public rootPage: any;
  

  constructor(
    public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public restProvider: RestProvider
  ) {
    if (this.restProvider.getLocalStorage('TOKEN')) {
      this.rootPage = HomePage;
      this.initializeApp();
    } else {
      this.rootPage = LoginPage;
      this.initializeApp();
    }
    
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      if(this.platform.is('android')) {
        this.statusBar.styleBlackOpaque();
      }
      setTimeout(() => {
        this.splashScreen.hide();
      }, 3000)
    });
  }

}
