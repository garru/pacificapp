import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { DetailHistoryPage } from '../pages/detail-history/detail-history';
import { LoginPage } from '../pages/login/login';
import { CasePage } from '../pages/case/case';
import { RegisterPage } from '../pages/register/register';
import { PaymentPage } from '../pages/payment/payment';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { RestProvider } from '../providers/rest/rest';
import { ValidationService } from '../share/validation';
import { ValidationControlMessages } from '../share/validation.control-message';
import { QrcodePage } from '../pages/qrcode/qrcode';
import { SuccessPage } from '../pages/success/success';
import { HistoryPage } from '../pages/history/history';
import { OtpPage } from '../pages/otp/otp';
import { InformationPage } from '../pages/information/information';
import { UserinformationPage } from '../pages/userinformation/userinformation';
import { QRScanner } from '@ionic-native/qr-scanner';
import { ProductDetailPage } from '../pages/product-detail/product-detail';
import { MyQrCodePage } from '../pages/my-qr-code/my-qr-code';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { ChangePasswordPage } from '../pages/change-password/change-password';
import { PromoPage } from '../pages/promo/promo';
import { MenuPage } from '../pages/menu/menu';
import { FilterPage } from '../pages/filter/filter';
import { PayPromoPage } from '../pages/pay-promo/pay-promo';
import { ListPromoPage } from '../pages/list-promo/list-promo';
import { FileTransfer } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { Camera } from '@ionic-native/camera';
import { HistorySharePage } from '../pages/history-share/history-share';
import { HistoryTransactionPage } from '../pages/history-transaction/history-transaction';
import { Login_2Page } from '../pages/login-2/login-2';
import { WalletPage } from '../pages/wallet/wallet';
import { CardLinkingPage } from '../pages/card-linking/card-linking';
import { AddCardPage } from '../pages/add-card/add-card';
import { AddMoneyPage } from '../pages/add-money/add-money';
import { AddNewCardPage } from '../pages/add-new-card/add-new-card';
import { WithdrawalPage } from '../pages/withdrawal/withdrawal';
import { WithdrawalDetailPage } from '../pages/withdrawal-detail/withdrawal-detail';
import { WithdrawalRequestPage } from '../pages/withdrawal-request/withdrawal-request';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { ForgotPasswordPage } from '../pages/forgot-password/forgot-password';
import { SpinnerDialog } from '@ionic-native/spinner-dialog';
import { ChangeInfoPage } from '../pages/change-info/change-info';

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}
@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    DetailHistoryPage,
    LoginPage,
    CasePage,
    RegisterPage,
    PaymentPage,
    ValidationControlMessages,
    QrcodePage,
    SuccessPage,
    HistoryPage,
    OtpPage,
    InformationPage,
    UserinformationPage,
    ProductDetailPage,
    MyQrCodePage,
    ChangePasswordPage,
    PromoPage,
    MenuPage,
    FilterPage,
    PayPromoPage,
    ListPromoPage,
    HistorySharePage,
    HistoryTransactionPage,
    Login_2Page,
    WalletPage,
    CardLinkingPage,
    AddCardPage,
    AddMoneyPage,
    AddNewCardPage,
    WithdrawalPage,
    WithdrawalDetailPage,
    WithdrawalRequestPage,
    ForgotPasswordPage,
    ChangeInfoPage,
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    IonicModule.forRoot(MyApp, {
      backButtonText: ' ',
      iconMode: 'ios',
      modalEnter: 'modal-slide-in',
      modalLeave: 'modal-slide-out',
      swipeBackEnabled: false ,
      menuType: 'overlay',
      scrollAssist: false,
      autoFocusAssist: false,
    }),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      }
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    DetailHistoryPage,
    LoginPage,
    CasePage,
    RegisterPage,
    PaymentPage,
    ValidationControlMessages,
    QrcodePage,
    SuccessPage,
    HistoryPage,
    OtpPage,
    InformationPage,
    UserinformationPage,
    ProductDetailPage,
    MyQrCodePage,
    ChangePasswordPage,
    PromoPage,
    MenuPage,
    FilterPage,
    PayPromoPage,
    ListPromoPage,
    HistorySharePage,
    HistoryTransactionPage,
    Login_2Page,
    WalletPage,
    CardLinkingPage,
    AddCardPage,
    AddMoneyPage,
    AddNewCardPage,
    WithdrawalPage,
    WithdrawalDetailPage,
    WithdrawalRequestPage,
    ForgotPasswordPage,
    ChangeInfoPage,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    RestProvider,
    ValidationService,
    ValidationControlMessages,
    QRScanner,
    FileTransfer,
    File,
    Camera,
    InAppBrowser,
    Navigator,
    SpinnerDialog
    
  ]
})
export class AppModule { }
