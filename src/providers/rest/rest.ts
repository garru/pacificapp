import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AlertController } from 'ionic-angular';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/timeout';
import { Loading, LoadingController } from 'ionic-angular';
import * as moment from 'moment';
import { KEY_STORAGE } from '../../share/constant';
import { Events } from 'ionic-angular';

/*
  Generated class for the RestProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class RestProvider {
  public baseUrl = 'https://pacificgo.asia/';
  public loading: Loading;
  constructor(
    public http: HttpClient,
    private alertController: AlertController,
    public loadingCtrl: LoadingController,
    public event: Events
  ) {
    console.log('Hello RestProvider Provider');
  }

  public GET(url, data = null, mainUrl?, header?, token?): Observable<any> {
    return this.http.get(mainUrl ? mainUrl + url : this.baseUrl + url, { params: data, headers: this.getHeaders(header, token) }).map(res => {
      return res;
    }).catch(this.handleError);
  }

  public POST(url, data, mainUrl?, header?, token?) {
    return this.http.post(mainUrl ? mainUrl + url : this.baseUrl + url, data, { headers: this.getHeaders(header, token) }).map(res => {
      return res;
    })
  }

  public PUT(url, data, mainUrl?, header?, token?) {
    return this.http.put(mainUrl ? mainUrl + url : this.baseUrl + url, data, { headers: this.getHeaders(header, token) }).map(res => {
      return res;
    })
  }

  handleError = (error: Response) => {
    if(error['status'] === 401) {
      this.event.publish('user:logout');
    }
    return Observable.throw(error);
  }

  getHeaders(header?, token?) {
    let headers = {};
    if (header) {
      headers['Content-Type'] = 'application/json';
    }
    if (token) {
      headers['Authorization'] = 'Bearer ' + window.localStorage.getItem('TOKEN');
    }
    return new HttpHeaders(headers);
  }

  setLocalStorage(key, value) {
    window.localStorage.setItem(key, value);
  }

  getLocalStorage(key) {
    return window.localStorage.getItem(key);
  }

  removeLocalStorage(key) {
    return window.localStorage.removeItem(key);
  }

  showAlert(mess) {
    let arlet = this.alertController.create({
      title: "THÔNG BÁO",
      message: mess,
      buttons: [{
        text: "OK",
        role: "cancel"
      }]
    })
    arlet.present();
  }

  initLoading(message?) {
    this.loading = this.loadingCtrl.create({
      content: message
    });
  }

  showLoading(message?) {
    if (!this.isHasLoading()) {
      this.initLoading(message);
      this.loading.present();
    }
  }

  hideLoading() {
    if (this.isHasLoading()) {
      this.loading.dismiss();
    }
  }

  isHasLoading() {
    let loading = document.getElementsByTagName("ion-loading").length;
    return loading ? true : false;
  }

  getUserInfor() {
    return this.getLocalStorage('USER_INFOR') ? JSON.parse(this.getLocalStorage('USER_INFOR')) : {};
  }

  isJSONString(value) {
    try {
      JSON.parse(value);
    } catch (e) {
      return false;
    }
    return true;
  }

  fakeLoading(duration?) {
    !duration && (duration = 500);
    this.showLoading();
    setTimeout(() => {
      this.hideLoading();
    }, duration)
  }

  getUserInforItem(key) {
    return this.getUserInfor()[key];
  }
  updateUserInfor(key, value) {
    let user = this.getUserInfor();
    user[key] = value;
    window.localStorage.setItem('USER_INFOR', JSON.stringify(user));
  }

  setUser(res) {
    res['data']['token'] && this.setLocalStorage('TOKEN', res['data']['token']);
    res['data']['User'].discountPoint = parseFloat((res['data']['User'].discount/100000).toFixed(2));
    this.setLocalStorage('USER_INFOR', JSON.stringify(res['data']['User']));
    this.setLocalStorage(KEY_STORAGE.LOGIN_TIME, moment.utc().toISOString());
  }

  formatCreditCardNumber(string) {
    string = string.replace(/ /g, '');
    string = string.replace(/[^\d]/g, '').match(/.{1,4}/g);
    return string ? string.join(' ') : '';
  }

  formatCurrency(money) {
    money = money || 0;
    return money.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");
  }

  encodeBankAccount(bankNumber) {
    bankNumber = bankNumber.toString()
    const length = bankNumber.length;
    return bankNumber.substring(0, 3) + "x".repeat(length - 6) + bankNumber.substring(length - 4, length - 1)
  }

}
